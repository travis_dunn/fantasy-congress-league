// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// See Bootstrap includes for plugins (https://github.com/twbs/bootstrap-sass/blob/master/vendor/assets/javascripts/bootstrap.js)
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap/collapse
//= require bootstrap/dropdown
//= require bootstrap/alert
//= require bootstrap/affix
//= require bootstrap/collapse
//= require bootstrap/tab
//= require_tree .
