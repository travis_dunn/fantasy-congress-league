class Payment
  extend  ActiveRecord::Enum
  include Mongoid::Document

  field :amount, type: Float

  enum change: { credit: 1, debit: 2 }  
  field :change, type: Integer

  belongs_to :user, index: true

  validates :change, inclusion: { in: Payment::CHANGE }, presence: true
  validates :amount, numericality: { within: 10..500, only_integer: true }, presence: true
end