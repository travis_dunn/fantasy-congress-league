class Team
  include Mongoid::Document

  field :name, type: String

  validates :name, length: { minimum: 6, maximum: 128 }, presence: true
  validate :validates_league_slots, on: :create
  validate :validates_cash, on: :create

  belongs_to :league
  belongs_to :user

  private

    def validates_league_slots
      if league.teams.count >= league.slots
        errors.add :league_id, "must have open slots"
      end
    end

    def validates_cash
    end
end