class Term
  extend  ActiveRecord::Enum
  include Mongoid::Document

  field :state, type: String
  field :starts_at, type: Date
  field :ends_at, type: Date
  field :district, type: Integer

  enum chamber: { senator: 1, representative: 2 }
  field :chamber, type: Integer

  enum party: { democrat: 1, republican: 2, independent: 3 }
  field :party, type: Integer  

  enum rank: { senior: 1, junior: 2 }
  field :rank, type: Integer    

  belongs_to :legislator
end