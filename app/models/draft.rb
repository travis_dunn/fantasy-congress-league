class Draft
  include Mongoid::Document

  field :next_turn_at, type: Time

  belongs_to :league

  scope :overdue, -> { where(next_turn_at: {'$lt' => Time.current}) }

  before_create :set_next_turn_at

  private

    def set_next_turn_at
      self.next_turn_at = Time.current + (league.draft_minutes || 1440).minutes 
    end
end