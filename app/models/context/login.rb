class Login
  include ActiveModel::Model

  attr_accessor :remember, :email, :password

  def find
    @user = User.find_by_email(email)
  end

  def id
    @user && @user.id
  end
end