class Season
  include Mongoid::Document

  field :starts_at, type: Date
  field :ends_at, type: Date
end