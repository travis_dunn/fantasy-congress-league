class User
  extend  ActiveRecord::Enum
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword 

  field :username, type: String
  field :first_name, type: String
  field :last_name, type: String
  field :email, type: String
  field :password_digest, type: String
  field :bio, type: String
  field :party, type: String
  field :birthdate, type: Date
  field :state, type: String
  field :last_login_at, type: Time

  enum status: { registered: 1, active: 2, disabled: -1 }
  field :status, type: Integer, default: -> { User::STATUS.fetch :registered }

  validates :username, presence: true, length: {minimum: 4, maximum: 64}
  validates :email, presence: true, length: {maximum: 256}, uniqueness: true
  validate :validate_tos_agreement, on: :create 

  has_many :payments

  attr_accessor :tos_agreement

  has_secure_password

  class << self
    def find_by_email(email)
      where(email: email.downcase).first
    end
  end

  private

    def validate_tos_agreement
      errors.add :terms_of_service_agreement, "required" unless has_aggreed_to_tos?
    end

    def has_aggreed_to_tos?
      tos_agreement == '1'
    end
end