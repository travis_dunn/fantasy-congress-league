class Legislator
  extend  Dragonfly::Model
  extend  ActiveRecord::Enum
  include Mongoid::Document
  include ActionView::Helpers::DateHelper
  
  field :last_name, type: String
  field :first_name, type: String
  field :middle_name, type: String
  field :suffix, type: String
  field :religion, type: String
  field :birthdate, type: Date
  field :phone, type: String
  field :website, type: String
  field :facebook_id, type: String
  field :twitter_id, type: String
  field :youtube_id, type: String
  field :govtrack_id, type: String
  field :cspan_id, type: String

  enum status: { sitting: 1 }
  field :status, type: Integer, default: -> { Legislator::STATUS.fetch :sitting }

  enum gender: { male: 1, female: 2 }
  field :gender, type: Integer

  embeds_one :position
  has_many   :terms
  field      :terms_count, type: Integer, default: 0

  field :portrait_uid, type: String
  dragonfly_accessor :portrait do
    default 'public/system/portraits/default.jpeg'
  end

  validates :status, inclusion: { in: Legislator::STATUS }, presence: true
  validates :gender, inclusion: { in: Legislator::GENDER }, allow_nil: true

  scope :sitting, ->{ where(status: Legislator::STATUS.fetch(:sitting)) }
  scope :terms, ->(lt, gt){ where(terms_count: lt..gt) }

  class << self
    def filter(params={})
      legislators = Legislator.sitting
      legislators = legislators.where('$or' => [{first_name: /#{params[:name]}/i}, {last_name: /#{params[:name]}/i}]) unless params[:name].blank?
      legislators = legislators.where(gender: Legislator::GENDER.fetch(params[:gender])) unless params[:gender].blank?
      legislators = legislators.terms(params.fetch(:terms,'').split('_').first, params.fetch(:terms,'').split('_').last) unless params[:terms].blank?
      legislators = legislators.where(religion: params[:religion]) unless params[:religion].blank?
      legislators = legislators.where('position.party' => params[:party]) unless params[:party].blank?
      legislators = legislators.where('position.chamber' => params[:chamber]) unless params[:chamber].blank?
      legislators
    end
  end

  def full_name
    [first_name,middle_name,last_name,suffix].reject {|v| v.blank? }.join(' ')
  end
  def age
    birthdate && (Date.current.year - birthdate.year - (birthdate > Date.current ? 1 : 0))
  end
end