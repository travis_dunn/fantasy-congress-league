class League
  extend  ActiveRecord::Enum
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword 

  EXPIRATION_DATE = 1.week
  
  field :name, type: String
  field :description, type: String
  field :stake, type: Integer
  field :password_digest, type: String
  field :draft_minutes, type: Integer
  field :slots, type: Integer, default: 12
  field :dynasty, type: Integer, default: 0

  enum format: { ladder: 1, rotisserie: 2, tournament: 3 }  
  field :format, type: Integer, default: -> { League::FORMAT.fetch :ladder }  

  enum lineup: { always: 1, daily: 2, weekends: 3 }  
  field :lineup, type: Integer, default: -> { League::LINEUP.fetch :daily }  

  enum draft: { linear: 1, snake: 2, salary: 3, all_star: 4, autodraft: 0 }  
  field :draft, type: Integer, default: -> { League::DRAFT.fetch :linear }  
  field :salary_flex, type: Boolean, default: false

  enum payout: { winner_takes_all: 1, top_two: 2, top_three: 3, championship: 4, fifty_fifty: 5 }  
  field :payout, type: Integer

  enum status: { open: 1, drafting: 2, ready: 3, playing: 4, finished: 0, canceled: -1, expired: -2 }
  field :status, type: Integer, default: -> { League::STATUS.fetch :open }  

  enum season: { week: 1, month: 2, quarter: 2, semiannual: 3, triannual: 4, annual: 5, congress: 0 }
  field :season, type: Integer, default: -> { League::SEASON.fetch :semiannual }  

  validates :name, length: { minimum: 6, maximum: 128 }, presence: true
  validates :slots, numericality: { within: 2..50, only_integer: true }, presence: true
  validates :stake, numericality: { within: 1000..50000, only_integer: true }, allow_nil: true
  validates :stake, presence: true, unless: -> { payout.blank? }
  validates :draft_minutes, numericality: { within: 0..(60*24), only_integer: true }, allow_nil: true
  validates :format, inclusion: { in: League::FORMAT }, presence: true
  validates :draft, inclusion: { in: League::DRAFT }, presence: true
  validates :lineup, inclusion: { in: League::LINEUP }, presence: true
  validates :status, inclusion: { in: League::STATUS }, presence: true
  validates :payout, inclusion: { in: League::PAYOUT }, allow_nil: true
  validates :payout, presence: true, if: -> { stake && stake > 0 }
  validates :dynasty, numericality: { minimum: 0, only_integer: true }, presence: true

  embeds_one :formation, cascade_callbacks: true
  belongs_to :commissioner, class_name: 'User'
  has_many :seasons do
    def current
      sort(starts_at: -1).first
    end
  end
  has_many :drafts do
    def current
      sort(id: 1).first
    end
  end  
  has_many  :teams
  def users; teams.map(&:user); end

  scope :dynasty, -> { where(dynasty: {'$gt' => 0}) }
  scope :timed, -> { where(draft_minutes: {'$gt' => 0}) }
  scope :stakes, ->(lt, gt){ where(stake: lt..gt) }
  scope :slots, ->(lt, gt){ where(slots: lt..gt) }
  scope :expired, -> {where(status: League::STATUS.fetch(:open), 
                            created_at: {'$lt' => Date.current - League::EXPIRATION_DATE } )}

  has_secure_password validations: false

  accepts_nested_attributes_for :formation

  class << self
    def filter(params={})
      status = League::STATUS.fetch(params[:status] || :open)

      leagues = League.where(status: status)
      leagues = leagues.where(name: /#{params[:name]}/i) unless params[:name].blank?
      leagues = leagues.where(format: League::FORMAT.fetch(params[:league_format])) unless params[:league_format].blank?
      leagues = leagues.where(payout: League::PAYOUT.fetch(params[:payout])) unless params[:payout].blank?
      leagues = leagues.where(draft: League::DRAFT.fetch(params[:draft])) unless params[:draft].blank?
      leagues = leagues.where(season: League::SEASON.fetch(params[:season])) unless params[:season].blank?
      leagues = leagues.slots(params.fetch(:slots,'').split('_').first, params.fetch(:slots,'').split('_').last) unless params[:slots].blank?
      leagues = leagues.stakes(params.fetch(:stake,'').split('_').first, params.fetch(:stake,'').split('_').last) unless params[:stake].blank?
      leagues = leagues.dynasty unless params[:dynasty].blank?
      leagues = leagues.timed unless params[:timed].blank?
      leagues
    end
  end  

  def draft!
    drafting!
    drafts.create
  end

  def cash?; !stake.blank?; end
  def slotted?; teams.count >= slots; end
end