class Position
  include Mongoid::Document

  field :chamber, type: String
  field :party, type: String
  field :state, type: String
  field :ends_at, type: Date

  embedded_in :legislator
end