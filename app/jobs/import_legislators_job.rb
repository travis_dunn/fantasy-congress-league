class ImportLegislatorsJob < BaseJob
  include GovtrackSyncing

  attr_accessor :docs, :social_docs

  def perform
    import_legislators
  end  

  def import_legislators
    log "ImportLegislatorsJob starting"

    self.docs = YAML.load_file legislators_path
    self.social_docs = YAML.load_file legislators_social_path    

    time = Benchmark.realtime do
      docs.each do |doc|
        terms = Term.where(legislator_id: doc['id']['bioguide']).count

        legislator = build_legislator(doc)

        new_record = legislator.new_record?
        changed = legislator.changed?

        if legislator.save
          count :added if new_record
          count :updated if changed
          count :positions if Term.where(legislator_id: legislator.id).count > terms
        else
          log "Error adding legislator record: #{doc} #{legislator.errors.full_messages.join(', ')}"
        end
      end
    end

    log "added #{counted(:added)} new legislators, updated #{counted(:updated)}, #{counted(:positions)} positions (#{time} seconds)"
  end

  protected

    def priority; 8; end  

    def build_legislator(doc)
      legislator = find_or_new_legislator doc['id']['bioguide']
      legislator.first_name = doc['name']['first']
      legislator.middle_name = doc['name']['middle']
      legislator.last_name = doc['name']['last']
      legislator.suffix = doc['name']['suffix']
      legislator.religion = doc['bio']['religion']
      legislator.birthdate = doc['bio']['birthday']
      legislator.gender = get_gender(doc['bio']['gender'])
      legislator.govtrack_id = doc['id']['govtrack']
      legislator.cspan_id = doc['id']['cspan']

      build_terms legislator, doc['terms']

      legislator
    end    

    def build_terms(legislator, terms)
      return if terms.blank?
      terms.each do |doc|
        term = build_term(doc)
        if not legislator.terms.detect{|t| t.starts_at == term.starts_at }
          legislator.terms << term
        end
      end
      legislator.terms_count = terms.size
      legislator.position = build_position(legislator.position, legislator.terms.last)
      legislator.website = terms.last['url']
      legislator.phone = terms.last['phone']
    end

    def build_position(position, term)
      if position.blank? || term[:ends_at] > position.ends_at
        Position.new state: term.state,
                     party: term.party, 
                     chamber: term.chamber, 
                     ends_at: term.ends_at
      else
        position
      end
    end

    def build_term(doc)
      term = Term.new
      term.chamber = get_chamber(doc['type'])
      term.party = get_party(doc['party'])
      term.state = doc['state']
      term.starts_at = doc['start']
      term.ends_at = doc['end']
      term.district = doc['district']
      term
    end

    def find_or_new_legislator(bioguide_id)
      @@legislators ||= Legislator.all
      @@legislators.detect{|l| l.id == bioguide_id} || Legislator.new(_id: bioguide_id)
    end

    def get_gender(text)
      if text && text[0].downcase == 'm'
        :male
      elsif text && text[0].downcase == 'f'  
        :female
      end
    end

    def get_chamber(text)
      if text && text[0].downcase == 's'
        :senator
      elsif text && text[0].downcase == 'r'  
        :representative
      end
    end 

    def get_party(text)
      if text && text[0].downcase == 'r'
        :republican
      elsif text && text[0].downcase == 'd'  
        :democrat
      else
        :independent
      end
    end 
end