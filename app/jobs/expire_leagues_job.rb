class ExpireLeaguesJob < BaseJob
  include DelayedScheduling

  run_every 1.day

  def perform
    expire_leagues
  end  

  def expire_leagues
    log "ExpireLeaguesJob starting"

    expired_leagues.each do |league|
      uids = league.user_ids + [league.commissioner_id]
      uids.each { |id| LeagueMailer.delay.expired id, league.id }
      league.expire!
      log "Expired league #{@league.name}, notified #{uids.size} users"
    end
  end

  protected

    def priority; 10; end

  private

    def expired_leagues
      League.expired
    end
end