class DraftOverdueJob < BaseJob
  include DelayedScheduling

  run_every 1.hour

  def perform
    draft_overdue
  end  

  def draft_overdue
    log "DraftOverdueJob starting"

    overdue_drafts.each do |draft|

    end

  end

  protected

    def priority; 6; end

  private

    def overdue_drafts
      Draft.overdue.includes(:league)
    end  
end