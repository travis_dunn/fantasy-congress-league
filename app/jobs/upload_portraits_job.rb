class UploadPortraitsJob < BaseJob
  include GovtrackSyncing

  SYNC_ARGS = [
    '-avz',
    '--delete',
    '--delete-excluded',
    '--exclude *50px.jpeg',
    '--exclude *100px.jpeg',
    '--exclude *200px.jpeg',
    '--exclude *.png',
    '--exclude *.txt']  

  def perform
    update_portraits
  end

  def update_portraits
    upload_portraits
    assign_portraits
  end

  private

    def upload_portraits
      log "UploadPortraitsJob starting sync"
      ensure_sync_path

      time = Benchmark.realtime do
        Rsync.run(photo_url, sync_path, SYNC_ARGS) do |result|
          if result.success?
            log("UploadPortraitsJob pulled changes from #{source_path}") unless result.changes.blank?
          else
            log "UploadPortraitsJob failed sync: #{result.error}"
            raise result.error
          end
        end
      end
      log "UploadPortraitsJob finished sync (#{time} seconds)"
    end

    def assign_portraits
      log "UploadPortraitsJob assigning portraits"

      time = Benchmark.realtime do
        legislators = portraitless_legislators

        legislators.each do |legislator|
          img = sync_path.join("photos/#{legislator.govtrack_id}.jpeg")
          if File.exists?(img)
            legislator.portrait = Pathname.new(img) 
            legislator.save
            count :assigned
          else
            count :missing
          end
        end
      end
      log "UploadPortraitsJob finished assigning #{counted(:assigned)} portraits, #{counted(:missing)} missing (#{time} seconds)"
    end

    def portraitless_legislators
      Legislator.where(portrait_uid: nil).all
    end
end