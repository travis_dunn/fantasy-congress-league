require 'fileutils'

class SyncDataJob < BaseJob
  include DelayedScheduling
  include GovtrackSyncing

  run_every 1.day

  SYNC_ARGS = [
    '-avz',
    '--delete',
    '--delete-excluded',
    '--exclude **/text-versions/',
    '--exclude **/*.xml',
    '--exclude **/*.css']  

  def perform
    sync_files
    queue_update_jobs
  end

  def sync_files(congress_id=nil)
    @congress = congress_id if congress_id

    log "SyncDataJob starting sync (#{congress})"
    ensure_sync_path

    time = Benchmark.realtime do
      file_urls.each do |source_path|
        dest_path = legislator_url == source_path ? sync_path : sync_path.join(congress.to_s)
        Rsync.run(source_path, dest_path, SYNC_ARGS) do |result|
          if result.success?
            log("UpdateDataJob pulled changes from #{source_path}") unless result.changes.blank?
          else
            log "UpdateDataJob failed sync: #{result.error}"
            raise result.error
          end
        end
      end
    end
    log "UpdateDataJob finished sync (#{time} seconds)"
  end

  def max_attempts; 2; end

  private

    def queue_update_jobs
    end

    def file_urls
      [legislator_url, bill_url, amendment_url, vote_url]
    end
end
