module LegislatorsHelper
  def filtered_legislators_path(param, value)
    value = params[param] == (value || '').to_s ? nil : value
    legislators_path legislators_filter.merge("#{param}" => value)
  end

  def filtering_legislators?
    !legislators_filter.values.select {|v| !v.blank?}.empty?
  end

  def facebook_page_url(id)
    "https://facebook.com/#{id}"
  end

  def twitter_account_url(id)
    "https://twitter.com/#{id}"
  end  

  def youtube_channel_url(id)
    "http://youtube.com/channel/#{id}"
  end    

  private

    def legislators_filter
        {
            name: params[:name],
            gender: params[:gender],
            party: params[:party],
            chamber: params[:chamber],
            terms: params[:terms]
        }
    end
end