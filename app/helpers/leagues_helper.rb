module LeaguesHelper
  def filtered_leagues_path(param, value)
    value = params[param] == (value || '').to_s ? nil : value
    leagues_path leagues_filter.merge("#{param}" => value)
  end

  def filtering_leagues?
    !leagues_filter.values.select {|v| !v.blank?}.empty?
  end

  def commissioner_only(&block)
    yield if true
  end  

  private

    def leagues_filter
        {
            league_format: params[:league_format],
            season: params[:season],
            draft_format: params[:draft_format],
            lineup: params[:lineup],
            name: params[:name],
            slots: params[:slots],
            stake: params[:stake],
            formation: params[:formation],
            payout: params[:payout],
            dynasty: params[:dynasty],
            timed: params[:timed]
        }
    end
end