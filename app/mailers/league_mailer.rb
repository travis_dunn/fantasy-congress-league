class LeagueMailer < BaseMailer

  def canceled(league_id)
  end

  def expired(user_id, league_id)
    @user = User.find(user_id)
    @league_id = League.find(league_id)
    mail :to => @user.email,
         :from => sender,
         :subject => "League registration expired"
  end

  def started(email, league_id)
  end

  def ready(league_id)
  end
end