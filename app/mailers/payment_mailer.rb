class PaymentMailer < BaseMailer
  def withdrawal(payment_id)
    @payment = Payment.find(payment_id)
    mail :to => @payment.user.email,
         :from => sender,
         :subject => "Funds Withdrawn"
  end

  def deposit(payment_id)
    @payment = Payment.find(payment_id)
    mail :to => @payment.user.email,
         :from => sender,
         :subject => "Funds Deposited"
  end  
end