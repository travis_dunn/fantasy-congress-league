class DraftMailer < BaseMailer
  def turn(team_id)
    @team = Team.find(team_id)
    mail :to => @team.user.email,
         :from => sender,
         :subject => "Draft turn for #{@team.name}"
  end

  def ended(team_id)
    @team = Team.find(team_id)
    mail :to => @team.user.email,
         :from => sender,
         :subject => "Draft ended"
  end
end