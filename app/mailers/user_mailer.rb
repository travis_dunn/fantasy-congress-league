class UserMailer < BaseMailer
  def registered(user_id)
    @user = User.find(user_id)
    mail :to => @user.email,
         :from => sender,
         :subject => "Welcome to Fantasy Congress League"
  end
end