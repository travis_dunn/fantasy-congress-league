class BaseMailer < ActionMailer::Base

  protected

    def sender
      email = Rails.application.config.system_email
      "#{email} <#{email}>"
    end
end