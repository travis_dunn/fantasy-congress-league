class LegislatorsController < ApplicationController
  include Pagination

  def index
    @legislators = paginate Legislator.filter(params)
  end  

  def show
    @legislator = Legislator.find(params[:id])
  end
end