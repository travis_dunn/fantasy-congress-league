class TeamsController < ApplicationController
  def new
    @league = League.open.find(params[:league_id])
    @team = @league.teams.new
  end

  def create
    @league = League.open.find(params[:league_id])
    @team = @league.teams.new new_team_params

    if @team.save
      start_draft if @league.slotted?
      redirect_to league_path(@league), notice: "Your team was created and you've joined the league."
    else
      flash_model_error! @team
      Rails.logger.warn "Team creation failed: #{@team.errors.full_messages.join(', ')}"
      render :new
    end    
  end

  private

    def new_team_params
      params.fetch(:team, {}).permit(:name)
    end

    def start_draft
      @league.draft!
      if @league.drafting?
        DraftMailer.delay.turn nil
      elsif @league.ready?
        @league.teams.each do |team|
          DraftMailer.delay.ended team.id
        end
      end
    end
end