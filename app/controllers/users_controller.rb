class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new registration_params
    if @user.valid?
      current_user_login! @user.id
      UserMailer.delay.registered @user.id
      redirect_to edit_user_path, notice: "Your account has been created. Check your email."
    else
      flash_model_error! @user
      Rails.logger.warn "Registration failed: #{@user.errors.full_messages.join(', ')}"
      render :new
    end
  end

  def show
    redirect_to root_path unless @user = User.where(id: params[:id]).first
  end

  private

    def registration_params
      params.fetch(:user, {}).permit(:email, :password, :username, :tos_agreement, :first_name, :last_name)
    end
end