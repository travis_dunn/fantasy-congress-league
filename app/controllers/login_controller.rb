class LoginController < ApplicationController
  def new
    @login = Login.new
  end

  def create
    @login = Login.new login_params

    if @login.find
      current_user_login! @login.id
      redirect_to root_path
    else
      flash_login_error!
      Rails.logger.warn "Login failed for email (#{@login.email}) or password (#{@login.password.length})"
      render :new
    end
  end

  def destroy
    current_user_logout! @login.id
    redirect_to root_path
  end

  private

    def flash_login_error!
      flash[:error] = "Could not find a user with that email address and password."
    end

    def login_params
      params.fetch(:login, {}).permit(:email, :password, :remember)
    end
end