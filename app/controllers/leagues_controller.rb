class LeaguesController < ApplicationController
  include Pagination

  def index
    @leagues = paginate League.filter(params)
  end

  def show
    @league = League.find(params[:id])
  end

  def new
    @league = League.new
  end

  def create
    @league = League.new new_league_params
    if @league.save
      redirect_to league_path(@league), notice: "Your league has been created."
    else
      flash_model_error! @league
      Rails.logger.warn "League creation failed: #{@league.errors.full_messages.join(', ')}"
      render :new
    end
  end

  def destroy
    League.find(params[:id]).cancel!
    redirect_to root_path
  end

  private

    def new_league_params
      params.fetch(:league, {}).permit(:name, :description, :stake, :draft_minutes, :slots, 
        :dynasty, :format, :lineup, :draft_format, :salary_flex, :payout, :status, :season
      ).reject {|k,v| v.blank? }
    end
end