module CurrentUser
  extend ActiveSupport::Concern

  included do
    helper_method :current_user_id, :logged_in?
  end  

  def current_user_login!(user_id)
    session[:user_id] = user_id
  end

  def current_user_logout!
    reset_session
    reset_cookies
  end

  def session_id
    @session_id ||= request.session_options[:id]
  end

  def current_user_id
    session[:user_id]
  end

  def logged_in?
    current_user ? true : false
  end

  protected

    def current_user
      @current_user ||= user_from_session || user_from_login_cookie
    end

    def require_current_user
      return if current_user
      store_return_to
      redirect_to new_login_path, notice: 'Login required' and return 
    end

  private

    def login_token_cookie
      cookies[:remember]
    end

    def user_from_session
      User.where(id: current_user_id).first if current_user_id
    end

    def user_from_login_cookie
      User.remember(login_token_cookie) if login_token_cookie
    end

    def reset_cookies
      cookies.each { |kv| cookies.delete kv.first.to_sym }
    end      
end
