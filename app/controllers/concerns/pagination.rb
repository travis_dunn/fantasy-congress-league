module Pagination
  extend ActiveSupport::Concern

  included do
    helper_method :current_page, :next_page, :prior_page, :more_pages?, :prior_pages?
  end

  def paginate(criteria,sort=:created_at)  
    @paginated_documents = criteria.skip(page_limit * (current_page-1)).limit(page_limit).sort(sort:-1)
  end

  def page_limit
    params[:limit] ||= 25
  end

  def more_pages?
    @paginated_documents.to_a.size >= page_limit
  end

  def prior_pages?
    current_page > 1
  end

  def current_page
    params[:id] ? params[:id].to_i : 1
  end

  def next_page
    current_page + 1
  end

  def prior_page
    [current_page - 1, 1].max
  end
end