class PaymentsController < ApplicationController
  def index
    @user = User.new
    @payment = Payment.new
  end

  def create
    redirect_to payments_path
  end
end