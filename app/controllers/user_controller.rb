class UserController < ApplicationController
  def show
  end

  def edit
    @user = User.new
  end

  def update
    redirect_to edit_user_path, notice: "Your account has been updated."
  end
end