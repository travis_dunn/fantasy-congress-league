class ApplicationController < ActionController::Base
  include CurrentUser

  respond_to :html
  protect_from_forgery with: :exception

  protected

    def flash_model_error!(model)
      flash[:error] = model.errors.full_messages.join('; ')
    end
end
