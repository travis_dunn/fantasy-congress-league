require "rsync"

namespace :data do
  desc "Download latest legislative govtrack data"
  task sync: :environment do
    SyncDataJob.new.sync_files
  end 

  namespace :legislators do
    desc "Import legislator records from govtrack data"
    task import: :environment do
      ImportLegislatorsJob.new.import_legislators
    end

    desc "Upload legislator portrait photos"
    task portraits: :environment do
      UploadPortraitsJob.new.upload_portraits
    end    
  end
end