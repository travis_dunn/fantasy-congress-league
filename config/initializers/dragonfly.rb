Dragonfly.app.configure do
  plugin :imagemagick

  protect_from_dos_attacks true
  secret Rails.application.secrets.dragonfly

  url_format "/images/:job/:name"

  datastore :file,
            root_path: Rails.root.join('public/system/portraits'),
            server_root: Rails.root.join('public')

  fetch_file_whitelist [
    /public\/system\/portraits/
  ]                 
end

Dragonfly.logger = Rails.logger
Rails.application.middleware.use Dragonfly::Middleware

