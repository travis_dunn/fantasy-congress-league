require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
# require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
require "rails/test_unit/railtie"

require 'active_record'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FCL
  class Application < Rails::Application
    config.time_zone = 'Eastern Time (US & Canada)'  

    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += %W(#{config.root}/app/models/context)
    config.autoload_paths += %W(#{config.root}/app/models/terms)
    config.autoload_paths += %W(#{config.root}/app/jobs/concerns)
    config.autoload_paths += %W(#{config.root}/lib/gem_ext/mongoid)
    
    YAML.load_file(Rails.root.join('config','application.yml'))[Rails.env].each do |k,v|
      config.send "#{k}=", v
    end

    config.paperclip_defaults = {
      storage: :file
    }
  end
end
