Rails.application.routes.draw do
  resource  :help, only: [:show], controller: :help
  resource  :contact, only: [:show], controller: :contact
  resource  :login, only: [:new, :create, :destroy], controller: :login
  resource  :user,  only: [:show, :edit, :update], controller: :user
  resources :users, only: [:new, :create, :show], controller: :users
  resources :leagues, only: [:index, :new, :create, :show] do
    resources :teams, only: [:new, :create]
  end
  resources :payments, only: [:index, :create], controller: :payments
  resources :legislators, only: [:index, :show]
  
  root to: 'site#show'
end